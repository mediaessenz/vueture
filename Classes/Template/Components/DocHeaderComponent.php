<?php

namespace MEDIAESSENZ\Vueture\Template\Components;

class DocHeaderComponent extends \TYPO3\CMS\Backend\Template\Components\DocHeaderComponent
{
    /**
     * @var bool
     */
    protected $vueMenus = false;

    /**
     * Returns the abstract content of the docHeader as an array
     *
     * @return array
     */
    public function docHeaderContent()
    {
        return [
            'enabled' => $this->isEnabled(),
            'buttons' => $this->buttonBar->getButtons(),
            'menus' => $this->menuRegistry->getMenus(),
            'metaInformation' => $this->metaInformation,
            'vueMenus' => $this->isVueMenus(),
        ];
    }

    /**
     * @return bool
     */
    public function isVueMenus()
    {
        return $this->vueMenus;
    }

    /**
     * @return bool
     */
    public function getVueMenus()
    {
        return $this->vueMenus;
    }

    /**
     * @param bool $value
     * @return DocHeaderComponent
     */
    public function setVueMenus($value)
    {
        $this->vueMenus = $value;
        return $this;
    }

}
