<?php

namespace MEDIAESSENZ\Vueture\Template\Components\Buttons\Action;

use TYPO3\CMS\Backend\Template\Components\Buttons\AbstractButton;
use TYPO3\CMS\Backend\Template\Components\Buttons\ButtonInterface;

class VuetureButton extends AbstractButton implements ButtonInterface
{
    /**
     * additional attributes for vue
     * Use key => value pairs
     *
     * @var array
     */
    protected $additionalAttributes = [];

    /**
     * @var string
     */
    protected $moduleName;

    /**
     * @var string
     */
    protected $id;

    /**
     * Gets the name of the module.
     *
     * @return string
     */
    public function getModuleName()
    {
        return $this->moduleName;
    }

    /**
     * Sets the name of the module.
     *
     * @param string $moduleName
     * @return VuetureButton
     */
    public function setModuleName($moduleName)
    {
        $this->moduleName = $moduleName;
        return $this;
    }

    /**
     * Validates all set parameters of a button.
     *
     * @return bool
     */
    public function isValid()
    {
        return !empty($this->id) && $this->getType() === self::class && $this->getIcon() !== null;
    }

    /**
     * Returns the fully qualified class name of the button as a string
     *
     * @return string
     */
    public function getType()
    {
        return get_class($this);
    }

    /**
     * @return string
     */
    public function getId(): string
    {
        return $this->id;
    }

    /**
     * @param string $id
     * @return VuetureButton
     */
    public function setId(string $id): VuetureButton
    {
        $this->id = $id;
        return $this;
    }

    /**
     * Get Data attributes
     *
     * @return array
     */
    public function getAdditionalAttributes()
    {
        return $this->additionalAttributes;
    }

    /**
     * Set Data attributes
     *
     * @param array $additionalAttributes additional attributes to set
     *
     * @return $this
     */
    public function setAdditionalAttributes(array $additionalAttributes)
    {
        $this->additionalAttributes = $additionalAttributes;
        return $this;
    }

    /**
     * Set Data attribute
     *
     * @param array $additionalAttribute additional attribute to set
     *
     * @return $this
     */
    public function addAdditionalAttribute(array $additionalAttribute)
    {
        $this->additionalAttributes .= $additionalAttribute;
        return $this;
    }

    /**
     * RenderMethod of a button if accessed as string from fluid
     *
     * @return string
     */
    public function __toString()
    {
        return $this->render();
    }

    /**
     * Renders the markup for the button
     *
     * @return string
     */
    public function render()
    {
        $attributes = [
            'id' => $this->getId(),
            'class' => 'btn btn-default btn-sm ' . $this->getClasses(),
            'title' => $this->getTitle()
        ];

        $labelText = '';
        if ($this->showLabelText) {
            $labelText = ' ' . $this->title;
        }

        foreach ($this->dataAttributes as $attributeName => $attributeValue) {
            $attributes['data-' . $attributeName] = $attributeValue;
        }

        foreach ($this->additionalAttributes as $attributeName => $attributeValue) {
            $attributes[$attributeName] = $attributeValue;
        }

        $attributesString = '';
        foreach ($attributes as $key => $value) {
            $attributesString .= ' ' . htmlspecialchars($key) . '="' . htmlspecialchars($value) . '"';
        }

        return '<button ' . $attributesString . '>' . $this->getIcon()->render() . htmlspecialchars($labelText) . '</button>';
    }

    /**
     * Gets the button group.
     *
     * @return int
     */
    public function getGroup()
    {
        return 100;
    }
}