<?php

namespace MEDIAESSENZ\Vueture\Template;

use MEDIAESSENZ\Vueture\Template\Components\DocHeaderComponent;
use TYPO3\CMS\Core\Imaging\IconFactory;
use TYPO3\CMS\Core\Messaging\FlashMessageService;
use TYPO3\CMS\Core\Page\PageRenderer;
use TYPO3\CMS\Core\Utility\GeneralUtility;

class ModuleTemplate extends \TYPO3\CMS\Backend\Template\ModuleTemplate
{

    /**
     * @var DocHeaderComponent
     */
    protected $docHeaderComponent;

  /**
   * Class constructor
   * Sets up view and property objects
   *
   * @param PageRenderer $pageRenderer
   * @param IconFactory $iconFactory
   * @param FlashMessageService $flashMessageService
   *
   * @throws \TYPO3Fluid\Fluid\View\Exception\InvalidTemplateResourceException In case a template is invalid
   */
  public function __construct(
    PageRenderer $pageRenderer,
    IconFactory $iconFactory,
    FlashMessageService $flashMessageService
  ) {
        parent::__construct($pageRenderer, $iconFactory, $flashMessageService);
        $this->docHeaderComponent = GeneralUtility::makeInstance(DocHeaderComponent::class);
    }

    /**
     * Get the DocHeader
     *
     * @return DocHeaderComponent
     */
    public function getDocHeaderComponent()
    {
        return $this->docHeaderComponent;
    }

}
