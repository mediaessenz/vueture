<?php
namespace MEDIAESSENZ\Vueture\Controller;

use TYPO3\CMS\Extbase\Mvc\Controller\ActionController;
use HDNET\Autoloader\Annotation\Plugin;

/***************************************************************
 *  Copyright notice
 *
 *  (c) 2018 Alexander Grein <alexander.grein@gmail.com>
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

/**
 * Last Modified:
 * Date: 26.03.18
 * Time: 15:55
 */
class FrontendPluginController extends ActionController
{
    /**
     * @Plugin Frontend
     */
    public function indexAction()
    {

    }
}
