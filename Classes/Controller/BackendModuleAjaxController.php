<?php
declare(strict_types=1);

namespace MEDIAESSENZ\Vueture\Controller;

use MEDIAESSENZ\Vueture\Utility\HerosUtility;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use TYPO3\CMS\Core\Http\Response;

class BackendModuleAjaxController
{

    /**
     * @param ServerRequestInterface $request
     * @return ResponseInterface
     */
    public function routeAction(ServerRequestInterface $request): ResponseInterface
    {
        $postedParams = $request->getParsedBody() ?? [];
        if (!$postedParams['route']) {
            $postedParams['route'] = self::getBackendUserAuthentication()->getModuleData('vueture/route',
                    'ses') ?? 'home';
        } else {
            self::getBackendUserAuthentication()->pushModuleData('vueture/route', $postedParams['route']);
        }
        $response = new Response();
        $response->getBody()->write(json_encode($postedParams));

        return $response;
    }

    /**
     * @param ServerRequestInterface $request
     * @return ResponseInterface
     */
    public function herosAction(ServerRequestInterface $request): ResponseInterface
    {
        $postedParams = $request->getParsedBody() ?? [];

        $responseData = isset($postedParams['data']) ? HerosUtility::saveHeros($postedParams['data']) : HerosUtility::getHeros($_GET);
        $response = new Response();
        $response->getBody()->write(json_encode($responseData));

        return $response;
    }

    /**
     * @param ServerRequestInterface $request
     * @return ResponseInterface|static
     */
    public function addHeroAction(ServerRequestInterface $request): ResponseInterface
    {
        $postedParams = $request->getParsedBody() ?? [];

        $response = new Response();
        if (isset($postedParams['hero'])) {
            HerosUtility::addHero($postedParams['hero']);
            $response->getBody()->write(json_encode(HerosUtility::getHeros($_GET)));
            return $response;
        } else {
            $response->getBody()->write('No hero data found.');
            return $response->withStatus(500);
        }
    }

    /**
     * @param ServerRequestInterface $request
     * @return ResponseInterface
     */
    public function simpleAction(ServerRequestInterface $request): ResponseInterface
    {
        //$postedParams = $request->getParsedBody() ?? [];
        //$getParams = $request->getQuerystring() ?? [];
        $response = new Response();
        $response->getBody()->write(json_encode(['message' => 'This message comes from ' . __METHOD__]));
        return $response;
    }

    /**
     * @return mixed|\TYPO3\CMS\Core\Authentication\BackendUserAuthentication
     */
    protected static function getBackendUserAuthentication()
    {
        return $GLOBALS['BE_USER'];
    }

}
