<?php

namespace MEDIAESSENZ\Vueture\Controller;

use TYPO3\CMS\Extbase\Mvc\Controller\ActionController;
use TYPO3\CMS\Extbase\Mvc\View\JsonView;

class FrontendPluginAjaxController extends ActionController
{
    /**
     * @var string
     */
    protected $defaultViewObjectName = JsonView::class;

    public function indexAction()
    {
        $this->view->assign('value', [
            'message' => 'This message comes from ' . __METHOD__
        ]);
    }

}