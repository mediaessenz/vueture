<?php
namespace MEDIAESSENZ\Vueture\Controller;

use MEDIAESSENZ\Vueture\View\BackendTemplateView;
use MEDIAESSENZ\Vueture\Template\Components\Buttons\Action\VuetureButton;
use TYPO3\CMS\Backend\Template\Components\ButtonBar;
use TYPO3\CMS\Core\Authentication\BackendUserAuthentication;
use TYPO3\CMS\Core\Imaging\Icon;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Extbase\Mvc\Controller\ActionController;
use TYPO3\CMS\Extbase\Mvc\View\ViewInterface;
use TYPO3\CMS\Extbase\Mvc\Web\Routing\UriBuilder;
use TYPO3\CMS\Lang\LanguageService;


/***************************************************************
 *  Copyright notice
 *
 *  (c) 2018 Alexander Grein <alexander.grein@gmail.com>
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

/**
 * Last Modified:
 * Date: 26.03.18
 * Time: 15:55
 */
class BackendModuleController extends ActionController
{
    /**
     * View object name
     *
     * @var string
     */
    protected $defaultViewObjectName = BackendTemplateView::class;

    /**
     * BackendTemplateContainer
     *
     * @var BackendTemplateView
     */
    protected $view;
    
    /**
     * Set up the doc header properly here
     *
     * @param ViewInterface $view
     * @return void
     */
    protected function initializeView(ViewInterface $view)
    {
        if (in_array($this->actionMethodName,  ['simpleAction', 'advancedAction'])) {
            // This view uses an extended version of the backend template view
            // which add a <div id="vueture-backend-module-docheader-menu"></div> in the jump menu area

            // Uncomment the following line to add an example additional normal select jump menu
            // you also have to add simple and advanced action to allowed actions in ext_table.php
            //$this->addSelectBoxJumpMenu();

            // Add buttons
            $this->addDocheaderButtons($this->actionMethodName);

            // Add flash message
            $this->view->getModuleTemplate()->setFlashMessageQueue($this->controllerContext->getFlashMessageQueue());

            if ($view instanceof BackendTemplateView) {

                // Add settings defined in ext_typoscript_setup.txt as TYPO3.settings.vueture object
                $view->getModuleTemplate()->getPageRenderer()->addInlineSettingArray('vueture', $this->settings);

                // Add modal JS
                $view->getModuleTemplate()->getPageRenderer()->loadRequireJsModule('TYPO3/CMS/Backend/Modal');

                // Add global JS var TYPO3.lang with the labels for the module index action
                $view->getModuleTemplate()->getPageRenderer()->addInlineLanguageLabelFile('EXT:vueture/Resources/Private/Language/BackendModuleAdvanced/locallang.xlf', 'vueture_backend_module.action.advanced.', 'vueture_backend_module.action.advanced.');

                /*
                $view->getModuleTemplate()->getPageRenderer()->addInlineLanguageLabelArray([
                    'name' => $this->getLanguageService()->sL('name'),
                ]);
                */
            }
        }
    }

    public function simpleAction()
    {
    }

    public function advancedAction()
    {
    }

    /**
     * Generates a normal action menu
     */
    protected function addSelectBoxJumpMenu()
    {
        $menuItems = [
            'simple' => [
                'controller' => 'BackendModule',
                'action' => 'simple',
                'label' => $this->getLanguageService()->sL('LLL:EXT:vueture/Resources/Private/Language/BackendModuleSimple/locallang.xlf:mlang_tabs_tab')
            ],
            'advanced' => [
                'controller' => 'BackendModule',
                'action' => 'advanced',
                'label' => $this->getLanguageService()->sL('LLL:EXT:vueture/Resources/Private/Language/BackendModuleAdvanced/locallang.xlf:mlang_tabs_tab')
            ],
        ];

        $menu = $this->view->getModuleTemplate()->getDocHeaderComponent()->getMenuRegistry()->makeMenu();
        $menu->setIdentifier('vueture-backend-module-jump-menu');
        //$menu->setLabel('Vueture jump menu');

        foreach ($menuItems as $menuItemConfig) {
            $menuItem = $menu->makeMenuItem()
                ->setTitle($menuItemConfig['label'])
                ->setHref($this->getHref($menuItemConfig['controller'], $menuItemConfig['action']))
                ->setActive($this->request->getControllerName() === $menuItemConfig['controller'] && $this->request->getControllerActionName() === $menuItemConfig['action']);
            $menu->addMenuItem($menuItem);
        }

        $this->view->getModuleTemplate()->getDocHeaderComponent()->getMenuRegistry()->addMenu($menu);
    }

    /**
     * Registers the Icons into the docheader
     * @param string $action
     * @throws \InvalidArgumentException
     */
    protected function addDocheaderButtons($action)
    {
        /** @var ButtonBar $buttonBar */
        $buttonBar = $this->view->getModuleTemplate()->getDocHeaderComponent()->getButtonBar();
        $currentRequest = $this->request;
        $moduleName = $currentRequest->getPluginName();
        $getVars = $this->request->getArguments();
        $extensionName = $currentRequest->getControllerExtensionName();
        if (count($getVars) === 0) {
            $modulePrefix = strtolower('tx_' . $extensionName . '_' . $moduleName);
            $getVars = ['id', 'M', $modulePrefix];
        }

        // remove "false &&" to add a vue buttons to advanced module
        // to get it work you also have to uncomment this lines inside *BackendModule.js*
        // new Vue({
        //     methods: {
        //         onClick() {top.TYPO3.Modal.confirm('Info', 'this info comes from ...')}
        //     }
        // }).$mount('#vueture-backend-module-docheader-button-info');
        if (false && $this->request->getControllerName() === 'BackendModule') {
            if ($this->request->getControllerActionName() === 'advanced') {
                // Add info button
                $buttonBar->addButton($this->generateVuetureButton('info', 'actions-document-info', [], ['@click' => 'onClick']), ButtonBar::BUTTON_POSITION_RIGHT);
            }
        }

        // Add shortcut button
        $shortcutName = $this->getLanguageService()->sL('LLL:EXT:vueture/Resources/Private/Language/' . ($action === 'simpleAction' ? 'BackendModuleSimple' : 'BackendModuleAdvanced') . '/locallang.xlf:mlang_shortcut');
        $shortcutButton = $buttonBar->makeShortcutButton()
            ->setModuleName($moduleName)
            ->setDisplayName($shortcutName)
            ->setGetVariables($getVars);
        $buttonBar->addButton($shortcutButton);

    }

    /**
     * @param string $id
     * @param string $icon
     * @param array $dataAttributes
     * @param array $additionalAttributes
     * @param string $cssClasses
     * @param string $idPrefix
     * @return VuetureButton|object
     */
    protected function generateVuetureButton($id, $icon, $dataAttributes = [], $additionalAttributes = ['@click' => 'onClick'], $cssClasses = 'vueture-backend-module-docheader-button', $idPrefix = 'vueture-backend-module-docheader-button-')
    {
        $vuetureButton = GeneralUtility::makeInstance(VuetureButton::class);
        $vuetureButton
            ->setId($idPrefix . $id)
            ->setClasses($cssClasses)
            ->setIcon($this->view->getModuleTemplate()->getIconFactory()->getIcon($icon, Icon::SIZE_SMALL))
            ->setDataAttributes($dataAttributes)
            ->setAdditionalAttributes($additionalAttributes);

        return $vuetureButton;
    }

    /**
     * Create URI for backend action
     *
     * @param string $controller
     * @param string $action
     * @param array $parameters
     * @return string
     */
    protected function getHref($controller, $action, $parameters = [])
    {
        $uriBuilder = $this->objectManager->get(UriBuilder::class);
        $uriBuilder->setRequest($this->request);
        return $uriBuilder->reset()->uriFor($action, $parameters, $controller);
    }

    /**
     * @return BackendUserAuthentication
     */
    protected function getBackendUserAuthentication()
    {
        return $GLOBALS['BE_USER'];
    }

    /**
     * @return LanguageService
     */
    protected function getLanguageService()
    {
        return $GLOBALS['LANG'];
    }

}
