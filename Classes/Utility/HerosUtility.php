<?php

namespace MEDIAESSENZ\Vueture\Utility;

class HerosUtility
{
    const TYPO3_HEROS = [
        ['id' => 1, 'name' => 'Alexander Kellner', 'years' => 10, 'extensions' => 48],
        ['id' => 2, 'name' => 'Andreas Beutel', 'years' => 10, 'extensions' => 3],
        ['id' => 3, 'name' => 'Andreas Wolf', 'years' => 10, 'extensions' => 1],
        ['id' => 4, 'name' => 'Bastian Waidelich', 'years' => 10, 'extensions' => 4],
        ['id' => 5, 'name' => 'Ben van \'t Ende', 'years' => 10, 'extensions' => 1],
        ['id' => 6, 'name' => 'Benjamin Kott', 'years' => 10, 'extensions' => 3],
        ['id' => 7, 'name' => 'Benni Mack', 'years' => 10, 'extensions' => 7],
        ['id' => 8, 'name' => 'Claus Due', 'years' => 10, 'extensions' => 21],
        ['id' => 9, 'name' => 'Georg Ringer', 'years' => 10, 'extensions' => 57],
        ['id' => 10, 'name' => 'Gina Steiner', 'years' => 10, 'extensions' => 0],
        ['id' => 11, 'name' => 'Helmut Hummel', 'years' => 10, 'extensions' => 8],
        ['id' => 12, 'name' => 'Ingo Renner', 'years' => 10, 'extensions' => 23],
        ['id' => 13, 'name' => 'Jo Hasenau', 'years' => 10, 'extensions' => 6],
        ['id' => 14, 'name' => 'Jochen Rau', 'years' => 10, 'extensions' => 6],
        ['id' => 15, 'name' => 'Jochen Weiland', 'years' => 10, 'extensions' => 0],
        ['id' => 16, 'name' => 'Josef Glatz', 'years' => 10, 'extensions' => 4],
        ['id' => 17, 'name' => 'Karsten Dambekalns', 'years' => 10, 'extensions' => 4],
        ['id' => 18, 'name' => 'Kasper Skårhøj', 'years' => 10, 'extensions' => 100],
        ['id' => 19, 'name' => 'Marc Willmann', 'years' => 10, 'extensions' => 1],
        ['id' => 20, 'name' => 'Markus Klein', 'years' => 10, 'extensions' => 5],
        ['id' => 21, 'name' => 'Mathias Brodala', 'years' => 10, 'extensions' => 6],
        ['id' => 22, 'name' => 'Mathias Schreiber', 'years' => 10, 'extensions' => 4],
        ['id' => 23, 'name' => 'Michael Stucki', 'years' => 10, 'extensions' => 2],
        ['id' => 24, 'name' => 'Nicole Cordes', 'years' => 10, 'extensions' => 25],
        ['id' => 25, 'name' => 'Oliver Hader', 'years' => 10, 'extensions' => 7],
        ['id' => 26, 'name' => 'Olivier Dobberkau', 'years' => 10, 'extensions' => 2],
        ['id' => 27, 'name' => 'Patrick Lobacher', 'years' => 10, 'extensions' => 3],
        ['id' => 28, 'name' => 'Robert Lemke', 'years' => 10, 'extensions' => 23],
        ['id' => 29, 'name' => 'Steffen Kamper', 'years' => 10, 'extensions' => 33],
        ['id' => 30, 'name' => 'Susanne Moog', 'years' => 10, 'extensions' => 7],
        ['id' => 31, 'name' => 'Tim Lochmüller', 'years' => 10, 'extensions' => 11],
    ];

    const TYPO3_HEROS_PROPERY_TYPES = [
        'id' => 'int',
        'name' => 'string',
        'years' => 'int',
        'extensions' => 'int',
    ];

    const TYPO3_HEROS_TABLE_STATES_TO_STORE = ['query', 'limit', 'page', 'byColumn', 'orderBy', 'ascending'];

    const LIMIT = 10;

    const PAGE = 1;

    const ORDER_BY = 'name';

    const ASCENDING = true;


    /**
     * @param array $constrains
     * @return array
     */
    public static function getHeros($constrains = [])
    {
        $heros = self::getBackendUserAuthentication()->getModuleData('vueture/heros', 'ses') ?? self::TYPO3_HEROS;
        $count = count($heros);

        if (count(array_intersect(self::TYPO3_HEROS_TABLE_STATES_TO_STORE, array_flip($constrains))) > 0) {
            // query
            $query = strtolower($constrains['query'] ?? '');

            if ($query) {
                $heros = array_filter($heros, function($values) use ($query) {
                    return strpos(strtolower($values['name']), $query) !== false;
                });
                $count = count($heros);
            }

            // limit
            $limit = (int)($constrains['limit'] ?? self::LIMIT);

            // page
            $page = (int)($constrains['page'] ?? self::PAGE);

            // byColumn
            $byColumn = $constrains['byColumn'] ?? 0;

            // orderBy
            $orderBy = $constrains['orderBy'] ?? self::ORDER_BY;

            // ascending
            $ascending = (bool)($constrains['ascending'] ?? self::ASCENDING);

            // save table states in session
            self::getBackendUserAuthentication()->pushModuleData('vueture/heros/states', array_intersect_key($constrains, array_flip(self::TYPO3_HEROS_TABLE_STATES_TO_STORE)));

            // order heros
            uasort($heros, function ($a, $b) use ($orderBy, $ascending) {
                if (self::TYPO3_HEROS_PROPERY_TYPES[$orderBy] === 'string') {
                    return $ascending ? strnatcmp($a[$orderBy], $b[$orderBy]) : strnatcmp($b[$orderBy], $a[$orderBy]);
                } else {
                    return $a[$orderBy] == $b[$orderBy] ? 0 : ($a[$orderBy] - $b[$orderBy]) * ($ascending ? 1 : -1);
                }
            });

            // limit heros
            $heros = array_slice($heros, ($page - 1) * $limit, $limit);
        }

        return [
            'data' => $heros,
            'count' => $count
        ];
    }

    /**
     * @param array $data
     * @return array
     */
    public static function saveHeros($data)
    {
        $heros = self::getHeros();
        array_walk($heros['data'], function(&$values, &$key, $data) {
            foreach($data as $newValues) {
                if ((int)$newValues['id'] === $values['id']) {
                    $values = array_replace($values, $newValues);
                }
                foreach (self::TYPO3_HEROS_PROPERY_TYPES as $property => $type) {
                    settype($values[$property], $type);
                }
            };
        }, $data);

        if (count($data) > count($heros['data'])) {
            // Add new hero
            $newHero = $data[count($heros['data'])];
            foreach (self::TYPO3_HEROS_PROPERY_TYPES as $property => $type) {
                settype($newHero[$property], $type);
            }
            if ($newHero['id'] > 0 && trim($newHero['name']) !== '') {
                $heros['data'][] = $newHero;
                $heros['count'] += 1;
            }
        }

        self::getBackendUserAuthentication()->pushModuleData('vueture/heros', $heros['data']);

        return [
            'message' => 'All heros saved.'
        ];
    }

    /**
     * Add new hero
     * @param array $hero
     */
    public static function addHero($hero)
    {
        $heros = self::getHeros();
        $hero['id'] = count($heros) + 1;
        $heros['data'][] = $hero;
        self::getBackendUserAuthentication()->pushModuleData('vueture/heros', $heros['data']);
    }

    /**
     * @return mixed|\TYPO3\CMS\Core\Authentication\BackendUserAuthentication
     */
    protected static function getBackendUserAuthentication()
    {
        return $GLOBALS['BE_USER'];
    }

}
