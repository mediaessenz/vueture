<?php
if (!defined('TYPO3_MODE')) {
    die('Access denied.');
}

\TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin(
    'vueture',
    'Frontend',
    [
        \MEDIAESSENZ\Vueture\Controller\FrontendPluginController::class => 'index',
        \MEDIAESSENZ\Vueture\Controller\FrontendPluginAjaxController::class => 'index',
    ],
    [
        \MEDIAESSENZ\Vueture\Controller\FrontendPluginController::class => 'index',
        \MEDIAESSENZ\Vueture\Controller\FrontendPluginAjaxController::class => 'index',
    ]
);
