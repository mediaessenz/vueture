(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["chunk-common"],{

/***/ "./node_modules/cache-loader/dist/cjs.js?!./node_modules/babel-loader/lib/index.js!./node_modules/cache-loader/dist/cjs.js?!./node_modules/vue-loader/lib/index.js?!./src/components/Button.vue?vue&type=script&lang=js&":
/*!***************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/cache-loader/dist/cjs.js??ref--12-0!./node_modules/babel-loader/lib!./node_modules/cache-loader/dist/cjs.js??ref--0-0!./node_modules/vue-loader/lib??vue-loader-options!./src/components/Button.vue?vue&type=script&lang=js& ***!
  \***************************************************************************************************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony import */ var core_js_modules_es_array_iterator__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! core-js/modules/es.array.iterator */ \"./node_modules/core-js/modules/es.array.iterator.js\");\n/* harmony import */ var core_js_modules_es_array_iterator__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_es_array_iterator__WEBPACK_IMPORTED_MODULE_0__);\n/* harmony import */ var core_js_modules_web_dom_collections_iterator__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! core-js/modules/web.dom-collections.iterator */ \"./node_modules/core-js/modules/web.dom-collections.iterator.js\");\n/* harmony import */ var core_js_modules_web_dom_collections_iterator__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_web_dom_collections_iterator__WEBPACK_IMPORTED_MODULE_1__);\n/* harmony import */ var _Icon__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./Icon */ \"./src/components/Icon.vue\");\n\n\n//\n//\n//\n//\n//\n//\n//\n\n/* harmony default export */ __webpack_exports__[\"default\"] = ({\n  name: 'Button',\n  components: {\n    Icon: _Icon__WEBPACK_IMPORTED_MODULE_2__[\"default\"]\n  },\n  props: {\n    title: {\n      type: String,\n      default: ''\n    },\n    buttonClasses: {\n      type: String,\n      default: 'btn btn-default btn-sm vueture-backend-module-docheader-button'\n    },\n    iconIdentifier: {\n      type: String,\n      default: ''\n    }\n  },\n  methods: {\n    emitEvent: function emitEvent() {\n      this.$emit('emission', ...arguments);\n    }\n  }\n});\n\n//# sourceURL=webpack:///./src/components/Button.vue?./node_modules/cache-loader/dist/cjs.js??ref--12-0!./node_modules/babel-loader/lib!./node_modules/cache-loader/dist/cjs.js??ref--0-0!./node_modules/vue-loader/lib??vue-loader-options");

/***/ }),

/***/ "./node_modules/cache-loader/dist/cjs.js?!./node_modules/babel-loader/lib/index.js!./node_modules/cache-loader/dist/cjs.js?!./node_modules/vue-loader/lib/index.js?!./src/components/Icon.vue?vue&type=script&lang=js&":
/*!*************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/cache-loader/dist/cjs.js??ref--12-0!./node_modules/babel-loader/lib!./node_modules/cache-loader/dist/cjs.js??ref--0-0!./node_modules/vue-loader/lib??vue-loader-options!./src/components/Icon.vue?vue&type=script&lang=js& ***!
  \*************************************************************************************************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony import */ var core_js_modules_es_string_split__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! core-js/modules/es.string.split */ \"./node_modules/core-js/modules/es.string.split.js\");\n/* harmony import */ var core_js_modules_es_string_split__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_es_string_split__WEBPACK_IMPORTED_MODULE_0__);\n\n//\n//\n//\n//\n/* harmony default export */ __webpack_exports__[\"default\"] = ({\n  name: 'Icon',\n  props: {\n    srcPath: {\n      type: String,\n      default: '/typo3/sysext/core/Resources/Public/Icons/T3Icons'\n    },\n    identifier: {\n      type: String,\n      default: 'actions-document-new'\n    },\n    fileExtension: {\n      type: String,\n      default: 'svg'\n    },\n    size: {\n      type: String,\n      default: 'small'\n    },\n    state: {\n      type: String,\n      default: 'default'\n    },\n    additionalClasses: {\n      type: String,\n      default: ''\n    }\n  },\n  computed: {\n    src: function src() {\n      var subPath = this.identifier.split('-')[0];\n      return \"\".concat(this.srcPath, \"/\").concat(subPath, \"/\").concat(this.identifier, \".\").concat(this.fileExtension);\n    },\n    classes: function classes() {\n      return \"t3js-icon icon icon-size-\".concat(this.size, \" icon-state-\").concat(this.state, \" icon-\").concat(this.identifier, \" \").concat(this.additionalClasses);\n    }\n  }\n});\n\n//# sourceURL=webpack:///./src/components/Icon.vue?./node_modules/cache-loader/dist/cjs.js??ref--12-0!./node_modules/babel-loader/lib!./node_modules/cache-loader/dist/cjs.js??ref--0-0!./node_modules/vue-loader/lib??vue-loader-options");

/***/ }),

/***/ "./node_modules/cache-loader/dist/cjs.js?{\"cacheDirectory\":\"node_modules/.cache/vue-loader\",\"cacheIdentifier\":\"778e4926-vue-loader-template\"}!./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/cache-loader/dist/cjs.js?!./node_modules/vue-loader/lib/index.js?!./src/components/Button.vue?vue&type=template&id=015de462&scoped=true&":
/*!***********************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/cache-loader/dist/cjs.js?{"cacheDirectory":"node_modules/.cache/vue-loader","cacheIdentifier":"778e4926-vue-loader-template"}!./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/cache-loader/dist/cjs.js??ref--0-0!./node_modules/vue-loader/lib??vue-loader-options!./src/components/Button.vue?vue&type=template&id=015de462&scoped=true& ***!
  \***********************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"render\", function() { return render; });\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"staticRenderFns\", function() { return staticRenderFns; });\nvar render = function() {\n  var _vm = this\n  var _h = _vm.$createElement\n  var _c = _vm._self._c || _h\n  return _c(\n    \"button\",\n    {\n      class: _vm.buttonClasses,\n      attrs: { title: _vm.title },\n      on: { click: _vm.emitEvent }\n    },\n    [\n      _vm.iconIdentifier\n        ? _c(\"icon\", { attrs: { identifier: _vm.iconIdentifier } })\n        : _vm._e(),\n      _vm._t(\"default\")\n    ],\n    2\n  )\n}\nvar staticRenderFns = []\nrender._withStripped = true\n\n\n\n//# sourceURL=webpack:///./src/components/Button.vue?./node_modules/cache-loader/dist/cjs.js?%7B%22cacheDirectory%22:%22node_modules/.cache/vue-loader%22,%22cacheIdentifier%22:%22778e4926-vue-loader-template%22%7D!./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/cache-loader/dist/cjs.js??ref--0-0!./node_modules/vue-loader/lib??vue-loader-options");

/***/ }),

/***/ "./node_modules/cache-loader/dist/cjs.js?{\"cacheDirectory\":\"node_modules/.cache/vue-loader\",\"cacheIdentifier\":\"778e4926-vue-loader-template\"}!./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/cache-loader/dist/cjs.js?!./node_modules/vue-loader/lib/index.js?!./src/components/Icon.vue?vue&type=template&id=abc9f7ae&":
/*!*********************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/cache-loader/dist/cjs.js?{"cacheDirectory":"node_modules/.cache/vue-loader","cacheIdentifier":"778e4926-vue-loader-template"}!./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/cache-loader/dist/cjs.js??ref--0-0!./node_modules/vue-loader/lib??vue-loader-options!./src/components/Icon.vue?vue&type=template&id=abc9f7ae& ***!
  \*********************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"render\", function() { return render; });\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"staticRenderFns\", function() { return staticRenderFns; });\nvar render = function() {\n  var _vm = this\n  var _h = _vm.$createElement\n  var _c = _vm._self._c || _h\n  return _c(\"span\", { class: _vm.classes }, [\n    _c(\"span\", { staticClass: \"icon-markup\" }, [\n      _c(\"img\", { attrs: { src: _vm.src, width: \"16\", height: \"16\" } })\n    ])\n  ])\n}\nvar staticRenderFns = []\nrender._withStripped = true\n\n\n\n//# sourceURL=webpack:///./src/components/Icon.vue?./node_modules/cache-loader/dist/cjs.js?%7B%22cacheDirectory%22:%22node_modules/.cache/vue-loader%22,%22cacheIdentifier%22:%22778e4926-vue-loader-template%22%7D!./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/cache-loader/dist/cjs.js??ref--0-0!./node_modules/vue-loader/lib??vue-loader-options");

/***/ }),

/***/ "./src/assets/Vue.svg":
/*!****************************!*\
  !*** ./src/assets/Vue.svg ***!
  \****************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

eval("module.exports = __webpack_require__.p + \"Images/Vue.svg\";\n\n//# sourceURL=webpack:///./src/assets/Vue.svg?");

/***/ }),

/***/ "./src/components/Button.vue":
/*!***********************************!*\
  !*** ./src/components/Button.vue ***!
  \***********************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony import */ var _Button_vue_vue_type_template_id_015de462_scoped_true___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./Button.vue?vue&type=template&id=015de462&scoped=true& */ \"./src/components/Button.vue?vue&type=template&id=015de462&scoped=true&\");\n/* harmony import */ var _Button_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./Button.vue?vue&type=script&lang=js& */ \"./src/components/Button.vue?vue&type=script&lang=js&\");\n/* empty/unused harmony star reexport *//* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ \"./node_modules/vue-loader/lib/runtime/componentNormalizer.js\");\n\n\n\n\n\n/* normalize component */\n\nvar component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__[\"default\"])(\n  _Button_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__[\"default\"],\n  _Button_vue_vue_type_template_id_015de462_scoped_true___WEBPACK_IMPORTED_MODULE_0__[\"render\"],\n  _Button_vue_vue_type_template_id_015de462_scoped_true___WEBPACK_IMPORTED_MODULE_0__[\"staticRenderFns\"],\n  false,\n  null,\n  \"015de462\",\n  null\n  \n)\n\n/* hot reload */\nif (false) { var api; }\ncomponent.options.__file = \"src/components/Button.vue\"\n/* harmony default export */ __webpack_exports__[\"default\"] = (component.exports);\n\n//# sourceURL=webpack:///./src/components/Button.vue?");

/***/ }),

/***/ "./src/components/Button.vue?vue&type=script&lang=js&":
/*!************************************************************!*\
  !*** ./src/components/Button.vue?vue&type=script&lang=js& ***!
  \************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony import */ var _node_modules_cache_loader_dist_cjs_js_ref_12_0_node_modules_babel_loader_lib_index_js_node_modules_cache_loader_dist_cjs_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Button_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../node_modules/cache-loader/dist/cjs.js??ref--12-0!../../node_modules/babel-loader/lib!../../node_modules/cache-loader/dist/cjs.js??ref--0-0!../../node_modules/vue-loader/lib??vue-loader-options!./Button.vue?vue&type=script&lang=js& */ \"./node_modules/cache-loader/dist/cjs.js?!./node_modules/babel-loader/lib/index.js!./node_modules/cache-loader/dist/cjs.js?!./node_modules/vue-loader/lib/index.js?!./src/components/Button.vue?vue&type=script&lang=js&\");\n/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__[\"default\"] = (_node_modules_cache_loader_dist_cjs_js_ref_12_0_node_modules_babel_loader_lib_index_js_node_modules_cache_loader_dist_cjs_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Button_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__[\"default\"]); \n\n//# sourceURL=webpack:///./src/components/Button.vue?");

/***/ }),

/***/ "./src/components/Button.vue?vue&type=template&id=015de462&scoped=true&":
/*!******************************************************************************!*\
  !*** ./src/components/Button.vue?vue&type=template&id=015de462&scoped=true& ***!
  \******************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony import */ var _node_modules_cache_loader_dist_cjs_js_cacheDirectory_node_modules_cache_vue_loader_cacheIdentifier_778e4926_vue_loader_template_node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_cache_loader_dist_cjs_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Button_vue_vue_type_template_id_015de462_scoped_true___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../node_modules/cache-loader/dist/cjs.js?{\"cacheDirectory\":\"node_modules/.cache/vue-loader\",\"cacheIdentifier\":\"778e4926-vue-loader-template\"}!../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../node_modules/cache-loader/dist/cjs.js??ref--0-0!../../node_modules/vue-loader/lib??vue-loader-options!./Button.vue?vue&type=template&id=015de462&scoped=true& */ \"./node_modules/cache-loader/dist/cjs.js?{\\\"cacheDirectory\\\":\\\"node_modules/.cache/vue-loader\\\",\\\"cacheIdentifier\\\":\\\"778e4926-vue-loader-template\\\"}!./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/cache-loader/dist/cjs.js?!./node_modules/vue-loader/lib/index.js?!./src/components/Button.vue?vue&type=template&id=015de462&scoped=true&\");\n/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, \"render\", function() { return _node_modules_cache_loader_dist_cjs_js_cacheDirectory_node_modules_cache_vue_loader_cacheIdentifier_778e4926_vue_loader_template_node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_cache_loader_dist_cjs_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Button_vue_vue_type_template_id_015de462_scoped_true___WEBPACK_IMPORTED_MODULE_0__[\"render\"]; });\n\n/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, \"staticRenderFns\", function() { return _node_modules_cache_loader_dist_cjs_js_cacheDirectory_node_modules_cache_vue_loader_cacheIdentifier_778e4926_vue_loader_template_node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_cache_loader_dist_cjs_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Button_vue_vue_type_template_id_015de462_scoped_true___WEBPACK_IMPORTED_MODULE_0__[\"staticRenderFns\"]; });\n\n\n\n//# sourceURL=webpack:///./src/components/Button.vue?");

/***/ }),

/***/ "./src/components/Icon.vue":
/*!*********************************!*\
  !*** ./src/components/Icon.vue ***!
  \*********************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony import */ var _Icon_vue_vue_type_template_id_abc9f7ae___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./Icon.vue?vue&type=template&id=abc9f7ae& */ \"./src/components/Icon.vue?vue&type=template&id=abc9f7ae&\");\n/* harmony import */ var _Icon_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./Icon.vue?vue&type=script&lang=js& */ \"./src/components/Icon.vue?vue&type=script&lang=js&\");\n/* empty/unused harmony star reexport *//* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ \"./node_modules/vue-loader/lib/runtime/componentNormalizer.js\");\n\n\n\n\n\n/* normalize component */\n\nvar component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__[\"default\"])(\n  _Icon_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__[\"default\"],\n  _Icon_vue_vue_type_template_id_abc9f7ae___WEBPACK_IMPORTED_MODULE_0__[\"render\"],\n  _Icon_vue_vue_type_template_id_abc9f7ae___WEBPACK_IMPORTED_MODULE_0__[\"staticRenderFns\"],\n  false,\n  null,\n  null,\n  null\n  \n)\n\n/* hot reload */\nif (false) { var api; }\ncomponent.options.__file = \"src/components/Icon.vue\"\n/* harmony default export */ __webpack_exports__[\"default\"] = (component.exports);\n\n//# sourceURL=webpack:///./src/components/Icon.vue?");

/***/ }),

/***/ "./src/components/Icon.vue?vue&type=script&lang=js&":
/*!**********************************************************!*\
  !*** ./src/components/Icon.vue?vue&type=script&lang=js& ***!
  \**********************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony import */ var _node_modules_cache_loader_dist_cjs_js_ref_12_0_node_modules_babel_loader_lib_index_js_node_modules_cache_loader_dist_cjs_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Icon_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../node_modules/cache-loader/dist/cjs.js??ref--12-0!../../node_modules/babel-loader/lib!../../node_modules/cache-loader/dist/cjs.js??ref--0-0!../../node_modules/vue-loader/lib??vue-loader-options!./Icon.vue?vue&type=script&lang=js& */ \"./node_modules/cache-loader/dist/cjs.js?!./node_modules/babel-loader/lib/index.js!./node_modules/cache-loader/dist/cjs.js?!./node_modules/vue-loader/lib/index.js?!./src/components/Icon.vue?vue&type=script&lang=js&\");\n/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__[\"default\"] = (_node_modules_cache_loader_dist_cjs_js_ref_12_0_node_modules_babel_loader_lib_index_js_node_modules_cache_loader_dist_cjs_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Icon_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__[\"default\"]); \n\n//# sourceURL=webpack:///./src/components/Icon.vue?");

/***/ }),

/***/ "./src/components/Icon.vue?vue&type=template&id=abc9f7ae&":
/*!****************************************************************!*\
  !*** ./src/components/Icon.vue?vue&type=template&id=abc9f7ae& ***!
  \****************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony import */ var _node_modules_cache_loader_dist_cjs_js_cacheDirectory_node_modules_cache_vue_loader_cacheIdentifier_778e4926_vue_loader_template_node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_cache_loader_dist_cjs_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Icon_vue_vue_type_template_id_abc9f7ae___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../node_modules/cache-loader/dist/cjs.js?{\"cacheDirectory\":\"node_modules/.cache/vue-loader\",\"cacheIdentifier\":\"778e4926-vue-loader-template\"}!../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../node_modules/cache-loader/dist/cjs.js??ref--0-0!../../node_modules/vue-loader/lib??vue-loader-options!./Icon.vue?vue&type=template&id=abc9f7ae& */ \"./node_modules/cache-loader/dist/cjs.js?{\\\"cacheDirectory\\\":\\\"node_modules/.cache/vue-loader\\\",\\\"cacheIdentifier\\\":\\\"778e4926-vue-loader-template\\\"}!./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/cache-loader/dist/cjs.js?!./node_modules/vue-loader/lib/index.js?!./src/components/Icon.vue?vue&type=template&id=abc9f7ae&\");\n/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, \"render\", function() { return _node_modules_cache_loader_dist_cjs_js_cacheDirectory_node_modules_cache_vue_loader_cacheIdentifier_778e4926_vue_loader_template_node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_cache_loader_dist_cjs_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Icon_vue_vue_type_template_id_abc9f7ae___WEBPACK_IMPORTED_MODULE_0__[\"render\"]; });\n\n/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, \"staticRenderFns\", function() { return _node_modules_cache_loader_dist_cjs_js_cacheDirectory_node_modules_cache_vue_loader_cacheIdentifier_778e4926_vue_loader_template_node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_cache_loader_dist_cjs_js_ref_0_0_node_modules_vue_loader_lib_index_js_vue_loader_options_Icon_vue_vue_type_template_id_abc9f7ae___WEBPACK_IMPORTED_MODULE_0__[\"staticRenderFns\"]; });\n\n\n\n//# sourceURL=webpack:///./src/components/Icon.vue?");

/***/ }),

/***/ "./src/styles/backend-module.scss":
/*!****************************************!*\
  !*** ./src/styles/backend-module.scss ***!
  \****************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

eval("// extracted by mini-css-extract-plugin\n    if(false) { var cssReload; }\n  \n\n//# sourceURL=webpack:///./src/styles/backend-module.scss?");

/***/ }),

/***/ "./src/utilities/Typo3Environment.js":
/*!*******************************************!*\
  !*** ./src/utilities/Typo3Environment.js ***!
  \*******************************************/
/*! exports provided: pid, lang, notification, ajaxUrls, verbose, debug, vueDevTools, consolePrefix */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"pid\", function() { return pid; });\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"lang\", function() { return lang; });\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"notification\", function() { return notification; });\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"ajaxUrls\", function() { return ajaxUrls; });\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"verbose\", function() { return verbose; });\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"debug\", function() { return debug; });\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"vueDevTools\", function() { return vueDevTools; });\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"consolePrefix\", function() { return consolePrefix; });\n/* harmony import */ var _Typo3Utilities__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./Typo3Utilities */ \"./src/utilities/Typo3Utilities.js\");\n/* global TYPO3, top */\n\nvar pid = _Typo3Utilities__WEBPACK_IMPORTED_MODULE_0__[\"default\"].determineSelectedPageId();\nvar lang = _Typo3Utilities__WEBPACK_IMPORTED_MODULE_0__[\"default\"].getTypo3LanguageLabels(TYPO3.lang); // eslint-disable-next-line no-restricted-globals\n\nvar notification = top.TYPO3.Notification; // eslint-disable-next-line no-restricted-globals, prefer-destructuring\n\nvar ajaxUrls = top.TYPO3.settings.ajaxUrls;\nvar verbose = TYPO3.settings.vueture.verbose === '1';\nvar debug = TYPO3.settings.vueture.debug === '1';\nvar vueDevTools = TYPO3.settings.vueture.vueDevTools === '1';\nvar consolePrefix = '[vueture]';\n\n//# sourceURL=webpack:///./src/utilities/Typo3Environment.js?");

/***/ }),

/***/ "./src/utilities/Typo3Utilities.js":
/*!*****************************************!*\
  !*** ./src/utilities/Typo3Utilities.js ***!
  \*****************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"default\", function() { return Typo3Utilities; });\nclass Typo3Utilities {\n  /**\n   * extract typo3 language labels\n   * @param {Array} source\n   * @returns {Array}\n   */\n  static getTypo3LanguageLabels(source) {\n    var lang = [];\n\n    if (source && typeof source === 'object') {\n      Object.keys(source).forEach(key => {\n        var value = source[key];\n\n        if (typeof value === 'object') {\n          lang[key] = value[0].target || value[0].source;\n        } else {\n          lang[key] = value;\n        }\n      });\n    }\n\n    return lang;\n  }\n  /**\n   * Return selected page if found, otherwise return 0\n   * @returns {number}\n   */\n\n\n  static determineSelectedPageId() {\n    // eslint-disable-next-line radix,no-restricted-globals\n    return parseInt(top.TYPO3.Utility.getParameterFromUrl(top.TYPO3.Backend.ContentContainer.getUrl(), 'id'), 10) || 0;\n  }\n  /**\n   * insert a new element after an existing one in the DOM tree\n   * @param el\n   * @param referenceNode\n   */\n\n\n  static insertBefore(el, referenceNode) {\n    referenceNode.parentNode.insertBefore(el, referenceNode);\n  }\n  /**\n   * insert a new element before an existing one in the DOM tree\n   * @param el\n   * @param referenceNode\n   */\n\n\n  static insertAfter(el, referenceNode) {\n    referenceNode.parentNode.insertBefore(el, referenceNode.nextSibling);\n  }\n  /**\n   * Add dom element for mounting vue-routes menu\n   * @param {boolean} withRouterView\n   * @param {number} numberOfMenus\n   * @param {string} routerViewName\n   * Todo implement possibility to generate markup for more than one menu\n   */\n\n\n  static generateVueDocHeaderMenuMarkup() {\n    var withRouterView = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : false;\n    var numberOfMenus = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : 1;\n    var routerViewName = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : 'routesMenu';\n    var baseSelector = '.module-docheader-bar-column-left';\n    var leftColumnElement = document.querySelector(\"\".concat(baseSelector, \" > .form-inline\"));\n    var numberOfExistingMenus = document.querySelectorAll(\"\".concat(baseSelector, \" .form-group\")).length;\n    var moreThanOneMenu = numberOfMenus + numberOfExistingMenus > 1;\n    var leftColClasses = moreThanOneMenu ? 'col-xs-12 col-sm-4 col-md-3' : 'col-xs-12 col-sm-3';\n    var rightColClasses = moreThanOneMenu ? 'col-xs-12 col-sm-8 col-md-9' : 'col-xs-12 col-md-9';\n    var convertToRightElement = document.querySelector(\"\".concat(baseSelector, \".text-right\"));\n    var menuExists = !convertToRightElement;\n\n    if (menuExists === false) {\n      convertToRightElement.setAttribute('class', \"module-docheader-bar-column-right \".concat(rightColClasses, \" text-right\"));\n    }\n\n    var afterBeginFormInlineMarkup = menuExists ? '' : \"<div class=\\\"module-docheader-bar-column-left \".concat(leftColClasses, \"\\\"><div class=\\\"form-inline row\\\">\");\n    var beforeEndFormInlineMarkup = menuExists ? '' : '</div></div>';\n    leftColumnElement = leftColumnElement || document.querySelector(baseSelector);\n    var routerViewMarkup = withRouterView ? \"<router-view name=\\\"\".concat(routerViewName, \"\\\"></router-view>\") : '';\n    var markup = \"\".concat(afterBeginFormInlineMarkup, \"<div id=\\\"vueture-backend-module-docheader-menu\\\">\").concat(routerViewMarkup, \"</div>\").concat(beforeEndFormInlineMarkup);\n\n    if (menuExists) {\n      leftColumnElement.insertAdjacentHTML('beforeend', markup);\n    } else {\n      convertToRightElement.insertAdjacentHTML('beforebegin', markup);\n    }\n  }\n  /**\n   * Add dom elements for vue mounting\n   * @param {boolean} withRouterView\n   * @param {number} numberOfButtonGroupsLeft\n   * @param {number} numberOfButtonGroupsRight\n   */\n\n\n  static generateVueDocHeaderButtonsMarkup() {\n    var withRouterView = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : false;\n    var numberOfButtonGroupsLeft = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : 1;\n    var numberOfButtonGroupsRight = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : 1;\n    var leftButtonToolBarElement = document.querySelector('.module-docheader-bar-column-left > .btn-toolbar');\n    var rightButtonToolBarElement = document.querySelector('.module-docheader-bar-column-right > .btn-toolbar');\n    var beforeIdPrefix = 'vueture-backend-module-docheader-buttons-right-';\n    var beforeRouteViewPrefix = 'docheaderButtonsRight';\n    var afterIdPrefix = 'vueture-backend-module-docheader-buttons-left-';\n    var afterRouteViewPrefix = 'docheaderButtonsLeft';\n    var afterbeginMarkup = '';\n    var beforeendMarkup = '';\n\n    for (var i = 1; i <= numberOfButtonGroupsLeft; i++) {\n      beforeendMarkup += \"<div id=\\\"\".concat(afterIdPrefix + i, \"\\\" class=\\\"btn-group vueture-btn-group\\\" role=\\\"group\\\" aria-label=\\\"\\\">\").concat(withRouterView ? \"<router-view name=\\\"\".concat(afterRouteViewPrefix + i, \"\\\"></router-view>\") : '', \"</div>\");\n    }\n\n    for (var j = numberOfButtonGroupsRight; j > 0; j--) {\n      afterbeginMarkup += \"<div id=\\\"\".concat(beforeIdPrefix + j, \"\\\" class=\\\"btn-group vueture-btn-group\\\" role=\\\"group\\\" aria-label=\\\"\\\">\").concat(withRouterView ? \"<router-view name=\\\"\".concat(beforeRouteViewPrefix + j, \"\\\"></router-view>\") : '', \"</div>\");\n    }\n\n    rightButtonToolBarElement.insertAdjacentHTML('afterbegin', afterbeginMarkup);\n    leftButtonToolBarElement.insertAdjacentHTML('beforeend', beforeendMarkup);\n  }\n\n}\n\n//# sourceURL=webpack:///./src/utilities/Typo3Utilities.js?");

/***/ })

}]);