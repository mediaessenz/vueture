# Ext:Vueture
## Installation

    composer require mediaessenz/vueture

+ Installation im Installtool

## Frontend Plugin benötigt
+ EXT:vhs
+ EXT:typoscript_rendering

### Build-Chain Init

    cd Build/backend && yarn install
    cd Build/frontend && yarn install

## Verfügbare Scripte
    yarn build (production)
    yarn build-development (development)

## Nutzung der Standalone Vue-Developer-Tools im Backend (für Debugging)

#### ACHTUNG! Aktuell funktioniert das Backend-Debugging über die vue-devtools leider nicht: 

[https://github.com/vuejs/vue-devtools/issues/1073]

Wenn das oben genannte Problem behoben ist, sollte es so klappen:

+ Auskommentierten Codeblock nach `// devtools import is only needed for debugging` aktivieren
+ development-build erzeugen
+ TYPO3 Cache löschen
+ Auf der Konsole `./node_modules/.bin/vue-devtools` aufrufen

Im Anschluss sollte sich eine Electron-App mit Debug-Informationen öffnen.

## Gut zu wissen

+ Die "Heros" werden in keiner eigenen Datenbank-Tabelle gespeichert, sondern nur in einer (Backend-)Session-Variable, die nach einem Logout wieder gelöscht wird.
+ Beim erneuten einloggen werden die Daten auf den im HeroService definierten Zustand zurückgesetzt

## Bekannte Problem

+ Ajax-Calls im Frontend mit EXT:typoscript_rendering bei gleichzeitig installierter EXT:frontend_editing funktioniert, wenn man im Backend eingeloggt ist, nur im Inkognito Tab oder anderen Browser
