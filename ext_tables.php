<?php
defined('TYPO3_MODE') or die();

if (TYPO3_MODE === 'BE') {

    // Register the backend module group
    \TYPO3\CMS\Extbase\Utility\ExtensionUtility::registerModule(
        'MEDIAESSENZ.Vueture',
        'vueture',
        '',
        '',
        [],
        [
            'access' => 'admin',
            'icon' => 'EXT:vueture/Resources/Public/Icons/backend-module-group-icon.svg',
            'labels' => 'LLL:EXT:vueture/Resources/Private/Language/BackendModuleGroup/locallang.xlf',
            /* Comment the next two lines to disable navigation frame */
            //'navigationComponentId' => '',
            //'inheritNavigationComponentFromMainModule' => false,
        ]
    );

    // Register the simple backend module
    \TYPO3\CMS\Extbase\Utility\ExtensionUtility::registerModule(
        'MEDIAESSENZ.Vueture',
        'vueture',
        'simple',
        '',
        [
            'BackendModule' => 'simple',
        ],
        [
            'access' => 'admin',
            'icon' => 'EXT:vueture/Resources/Public/Icons/Extension.svg',
            'labels' => 'LLL:EXT:vueture/Resources/Private/Language/BackendModuleSimple/locallang.xlf',
            /* Comment the next two lines to disable navigation frame */
            //'navigationComponentId' => '',
            //'inheritNavigationComponentFromMainModule' => false,
        ]
    );

    // Register the advanced backend module with vuex and router
    \TYPO3\CMS\Extbase\Utility\ExtensionUtility::registerModule(
        'MEDIAESSENZ.Vueture',
        'vueture',
        'advanced',
        '',
        [
            'BackendModule' => 'advanced',
        ],
        [
            'access' => 'admin',
            'icon' => 'EXT:vueture/Resources/Public/Icons/Extension.svg',
            'labels' => 'LLL:EXT:vueture/Resources/Private/Language/BackendModuleAdvanced/locallang.xlf',
            /* Comment the next two lines to disable navigation frame */
            //'navigationComponentId' => '',
            //'inheritNavigationComponentFromMainModule' => false,
        ]
    );

    // Add typoscript template
    \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addTypoScript(
        'vueture',
        'setup',
        '<INCLUDE_TYPOSCRIPT: source="FILE:EXT:vueture/Configuration/TypoScript/setup.txt">'
    );
}

\TYPO3\CMS\Extbase\Utility\ExtensionUtility::registerPlugin('vueture', 'Frontend', 'Vueture');