import Vue from 'vue';

export default class CompileRender {

  /**
   * Compile Render Example
   * @param elementId
   * @param message
   * @returns {*}
   */
  constructor(elementId, message) {
    let templateRenderFns = [];
    let template = `<div id="${elementId}">${document.getElementById(elementId).innerHTML}</div>`;

    new Vue({
      data: {
        templateRender: null,
        message: message
      },
      mounted() {
        let compiledTemplate = Vue.compile(template);
        this.templateRender = compiledTemplate.render;
        templateRenderFns = [];
        if (compiledTemplate.staticRenderFns.length > 0) {
          for (let i in compiledTemplate.staticRenderFns) {
            templateRenderFns.push(compiledTemplate.staticRenderFns[i]);
          }
        }
      },
      methods: {
        reverseMessage: function () {
          this.message = this.message.split('').reverse().join('');
        }
      },
      staticRenderFns: templateRenderFns,
      render(h) {
        return h('div', [this.templateRender ? this.templateRender() : '']);
      }
    }).$mount('#' + elementId);

  }

}
