module.exports = {
  outputDir: '../../Resources/Public/Backend',
  publicPath: '/typo3conf/ext/vueture/Resources/Public/Backend/',
  filenameHashing: false,
  productionSourceMap: false,
  runtimeCompiler: true,
  configureWebpack: {
    externals: {
      // shows how we can rely on browser globals instead of bundling these dependencies,
      // in case we want to access jQuery from a CDN or if we want an easy way to
      // avoid loading all moment locales: https://github.com/moment/moment/issues/1435
      // jquery: 'jQuery',
      // moment: 'moment',
    },
    entry: {
      simple: './src/simple.js',
      advanced: './src/advanced.js',
    },
    output: {
      filename: process.env.VUE_CLI_MODERN_BUILD ? 'JavaScript/[name].js' : 'JavaScript/[name]-legacy.js',
      chunkFilename: process.env.VUE_CLI_MODERN_BUILD ? 'JavaScript/[name].js' : 'JavaScript/[name]-legacy.js',
    },
  },
  chainWebpack: (config) => {
    config.entryPoints.delete('app');
    config.module.rule('images').use('url-loader').loader('url-loader').tap((options) => {
      options.fallback.options.name = 'Images/[name].[ext]';
      return options;
    });
    config.module.rule('svg').use('file-loader').loader('file-loader').tap((options) => {
      options.name = 'Images/[name].[ext]';
      return options;
    });
    config.module.rule('media').use('url-loader').loader('url-loader').tap((options) => {
      options.fallback.options.name = 'Media/[name].[ext]';
      return options;
    });
    config.module.rule('fonts').use('url-loader').loader('url-loader').tap((options) => {
      options.fallback.options.name = 'Fonts/[name].[ext]';
      return options;
    });
  },

  css: {
    extract: {
      filename: 'Css/[name].css',
      chunkFilename: 'Css/[name].css',
    },
  },

};
