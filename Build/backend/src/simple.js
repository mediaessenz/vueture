import Vue from 'vue';
import { verbose, consolePrefix, notification, ajaxUrls } from './utilities/Typo3Environment';
import HelloWorld from './components/HelloWorldSimple';
import './styles/backend-module.scss';

// devtools import is only needed for debugging
// import devtools from '@vue/devtools';
// devtools.connect();

document.addEventListener('DOMContentLoaded', () => {
  if (verbose) {
    // eslint-disable-next-line no-console
    console.log(`${consolePrefix} [loader] dom ready`);
  }

  new Vue({
    data: {
      title: 'Hello Vueture!',
      message: 'Loading ...'
    },
    created() {
      fetch(ajaxUrls['vueture/simple']).then(response => response.json()).then((responseJson) => {
        this.message = responseJson.message;
      }).catch((error) => {
        notification.error('Backend module controller feedback', error);
      });
      /*
      axios.get(ajaxUrls['vueture/simple']).then((response) => {
          this.message = response.data.message;
      }).catch((error) => {
          notification.error('Backend module controller feedback', error.request.response);
      });
      */
    },
    render(h) {
      return h(HelloWorld, {
        props: {
          title: this.title,
          message: this.message
        }
      });
    }
  }).$mount('#vueture-backend-module-content');

  // eslint-disable-next-line no-restricted-globals
  top.TYPO3.Backend.Loader.finish();

});
