import Vue from 'vue';
import Vuex from 'vuex';

Vue.use(Vuex);
import qs from 'qs';
import router from '../router';
import { lang, notification, ajaxUrls, verbose, consolePrefix } from '../utilities/Typo3Environment';

const routesMenuModule = {
  namespaced: true,
  state: {
    routes: [
      {text: 'Home', route: 'home'},
      {text: 'Example 1', route: 'example-1'},
      {text: 'TYPO3 Heros V1', route: 'example-2'},
      {text: 'TYPO3 Heros V2', route: 'example-3'}
    ],
    route: 'home'
  },
  getters: {
    routeO: state => {
      return state.routes.filter(function (obj) {
        return obj.route === state.route;
      })[0];
    }
  },
  mutations: {
    changeRoute(state, route) {
      state.route = route;
    }
  },
  actions: {
    SAVE_ROUTE({commit, state, dispatch}, route) {
      // RoutesMenu.vue & RoutesMenuAdvanced.vue use different params
      route = route.route || route.target.value;
      if (verbose) {
        // eslint-disable-next-line no-console
        console.log('SAVE_ROUTE', route);
      }
      commit('changeRoute', route);

      fetch(ajaxUrls['vueture/route'], {
        method: 'POST',
        mode: 'cors',
        cache: 'no-cache',
        credentials: 'same-origin',
        headers: new Headers({
          'Accept': 'application/json',
          'Content-Type': 'application/x-www-form-urlencoded; charset=utf-8'
        }),
        redirect: 'follow',
        referrer: 'no-referrer',
        body: qs.stringify({
          route: state.route
        })
      }).then(response => {
        if (response.ok) {
          return response.json();
        } else {
          throw new Error(response.statusText);
        }
      }).then(responseJson => {
        // eslint-disable-next-line no-console
        console.log(responseJson);
        dispatch('CHANGE_ROUTE', responseJson.route);
      }).catch(error => {
        notification.error('Backend module controller feedback', error);
      });

      /*
      axios.post(ajaxUrls['vueture/route'], qs.stringify({route: state.route}), {
          validateStatus: function (status) {
              return status < 500;
          }
      }).then((response) => {
          dispatch('CHANGE_ROUTE', response.data.route);
      }).catch((error) => {
          notification.error('Backend module controller feedback', error.request.response);
      });
      */
    },
    CHANGE_ROUTE({commit, dispatch}, route) {
      if (verbose) {
        // eslint-disable-next-line no-console
        console.log(consolePrefix + 'change route to', route);
      }
      commit('changeRoute', route);
      if (route === 'example-2') {
        dispatch('typo3HerosOneModule/LOAD_HEROS', null, {root: true}).then(() => router.push({name: route}));
      } else {
        router.push({name: route});
      }
    }
  }
};

const helloWorldModule = {
  namespaced: true,
  state: {
    logoSize: 400
  },
  mutations: {
    incrementLogoSize: (state, amount) => state.logoSize += amount,
    decrementLogoSize: (state, amount) => state.logoSize -= amount
  }
};

const typo3HerosOneModule = {
  namespaced: true,
  state: {
    heros: [],
    showFilter: false,
    showNewHeroInput: false,
    newHero: {}
  },
  mutations: {
    changeHeros: (state, heros) => state.heros = heros,
    filter: (state, visible) => state.showFilter = visible,
    changeShowNewHeroInput: (state, visible) => state.showNewHeroInput = visible,
    addHero: (state, hero) => state.heros.push(hero),
    emptyNewHero: state => state.newHero = {}
  },
  actions: {
    ADD_NEW_HERO({commit, state}) {
      commit('changeShowNewHeroInput', !state.showNewHeroInput);
      //notification.info('Info', 'New button in docheader was clicked!');
    },
    TOGGLE_HEROS_FILTER({commit, state}) {
      commit('filter', !state.showFilter);
    },
    LOAD_HEROS({commit}) {
      fetch(ajaxUrls['vueture/heros']).then(response => response.json()).then(responseJson => {
        commit('changeHeros', responseJson.data);
      }).catch(error => {
        notification.error('Backend module controller feedback', error);
      });
      /*
      axios.get(ajaxUrls['vueture/heros']).then((response) => {
          commit('changeHeros', response.data.data);
      }).catch((error) => {
          notification.error('Backend module controller feedback', error.request.response);
      })
      */
    },
    SAVE_HEROS({commit, state}) {
      if (verbose) {
        notification.info('Vuex store save action called', lang.ajaxMessagePing);
      }

      // Add new hero
      if (typeof state.newHero.name === 'string' && state.newHero.name !== '') {
        let nextId = Math.max.apply(Math, state.heros.map(o => o.id)) + 1;
        // console.log('highestID', nextId);
        state.newHero.id = nextId;
        // console.log('Save new hero ...', state.newHero);
        commit('addHero', state.newHero);
        commit('emptyNewHero');
        commit('changeShowNewHeroInput', false);
      }

      fetch(ajaxUrls['vueture/heros'], {
        method: 'POST',
        mode: 'cors',
        cache: 'no-cache',
        credentials: 'same-origin',
        headers: new Headers({
          'Accept': 'application/json',
          'Content-Type': 'application/x-www-form-urlencoded; charset=utf-8'
        }),
        redirect: 'follow',
        referrer: 'no-referrer',
        body: qs.stringify({
          data: state.heros
        })
      }).then(response => {
        if (response.ok) {
          return response.json();
        } else {
          throw new Error(response.statusText);
        }
      }).then(responseJson => {
        // console.log(responseJson);
        notification.success('Backend module controller feedback', responseJson.message);
      }).catch(error => {
        notification.error('Backend module controller feedback', error);
      });
      /*
      axios.post(ajaxUrls['vueture/heros'], qs.stringify({ data: state.heros }), {
      validateStatus: function (status) {
      return status < 500;
      }}).then((response) => {
      notification.success('Backend module controller feedback', response.data.message);
      }).catch((error) => {
      notification.error('Backend module controller feedback', error.request.response);
      });
      */
    }
  }
};


export default new Vuex.Store({
  modules: {
    routesMenuModule,
    helloWorldModule,
    typo3HerosOneModule
  },
  state: {
    herosTable: {
      query: '',
      page: 1
    }
  },
  mutations: {
    ['herosStore/SORTED'](state, data) {
      if (verbose) {
        // eslint-disable-next-line no-console
        console.log('herosStore/SORTED', data);
      }
    },
    ['herosStore/LOADED'](state, data) {
      if (verbose) {
        // eslint-disable-next-line no-console
        console.log('herosStore/LOADED', data);
      }
    },
    ['herosStore/PAGINATION'](state, data) {
      if (verbose) {
        // eslint-disable-next-line no-console
        console.log('herosStore/PAGINATION', data);
      }
    },
    ['herosStore/LIMIT'](state, data) {
      if (verbose) {
        // eslint-disable-next-line no-console
        console.log('herosStore/LIMIT', data);
      }
    },
    ['herosStore/FILTER'](state, data) {
      if (verbose) {
        // eslint-disable-next-line no-console
        console.log('herosStore/FILTER', data);
      }
      state.herosTable.query = data;
    },
    ['herosTable1/PAGINATE'](state, page) {
      if (verbose) {
        // eslint-disable-next-line no-console
        console.log('herosTable1/PAGINATE', state, page);
      }
      state.herosTable.page = page;
    }
  },
  actions: {
    REFRESH() {
      top.TYPO3.Backend.Loader.start();
      if (top.TYPO3.Backend.NavigationContainer.PageTree !== null) {
        top.TYPO3.Backend.NavigationContainer.PageTree.refreshTree();
      } else {
        // for TYPO3 9 use the following command to refresh the navigation tree
        top.TYPO3.Backend.NavigationContainer.refresh();
      }
      top.TYPO3.Backend.ContentContainer.refresh();
    }
  }
});
