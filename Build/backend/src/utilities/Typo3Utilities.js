export default class Typo3Utilities {
  /**
   * extract typo3 language labels
   * @param {Array} source
   * @returns {Array}
   */
  static getTypo3LanguageLabels(source) {
    const lang = [];
    if (source && typeof source === 'object') {
      Object.keys(source).forEach((key) => {
        const value = source[key];
        if (typeof value === 'object') {
          lang[key] = value[0].target || value[0].source;
        } else {
          lang[key] = value;
        }
      });
    }
    return lang;
  }

  /**
   * Return selected page if found, otherwise return 0
   * @returns {number}
   */
  static determineSelectedPageId() {
    // eslint-disable-next-line radix,no-restricted-globals
    if (top.TYPO3.Utility !== undefined) {
      return parseInt(top.TYPO3.Utility.getParameterFromUrl(top.TYPO3.Backend.ContentContainer.getUrl(), 'id'), 10) || 0;
    } else {
      // eslint-disable-next-line no-console
      console.warn('top.TYPO3.Utility is undefined!');
      return 0;
    }
  }

  /**
   * insert a new element after an existing one in the DOM tree
   * @param el
   * @param referenceNode
   */
  static insertBefore(el, referenceNode) {
    referenceNode.parentNode.insertBefore(el, referenceNode);
  }

  /**
   * insert a new element before an existing one in the DOM tree
   * @param el
   * @param referenceNode
   */
  static insertAfter(el, referenceNode) {
    referenceNode.parentNode.insertBefore(el, referenceNode.nextSibling);
  }

  /**
   * Add dom element for mounting vue-routes menu
   * @param {boolean} withRouterView
   * @param {number} numberOfMenus
   * @param {string} routerViewName
   * Todo implement possibility to generate markup for more than one menu
   */
  static generateVueDocHeaderMenuMarkup(withRouterView = false, numberOfMenus = 1, routerViewName = 'routesMenu') {
    const baseSelector = '.module-docheader-bar-column-left';
    let leftColumnElement = document.querySelector(`${baseSelector} > .form-inline`);
    const numberOfExistingMenus = document.querySelectorAll(`${baseSelector} .form-group`).length;
    const moreThanOneMenu = numberOfMenus + numberOfExistingMenus > 1;
    const leftColClasses = moreThanOneMenu ? 'col-xs-12 col-sm-4 col-md-3' : 'col-xs-12 col-sm-3';
    const rightColClasses = moreThanOneMenu ? 'col-xs-12 col-sm-8 col-md-9' : 'col-xs-12 col-md-9';
    const convertToRightElement = document.querySelector(`${baseSelector}.text-right`);
    const menuExists = !convertToRightElement;
    if (menuExists === false) {
      convertToRightElement.setAttribute('class', `module-docheader-bar-column-right ${rightColClasses} text-right`);
    }
    const afterBeginFormInlineMarkup = menuExists ? '' : `<div class="module-docheader-bar-column-left ${leftColClasses}"><div class="form-inline row">`;
    const beforeEndFormInlineMarkup = menuExists ? '' : '</div></div>';
    leftColumnElement = leftColumnElement || document.querySelector(baseSelector);
    const routerViewMarkup = withRouterView ? `<router-view name="${routerViewName}"></router-view>` : '';
    const markup = `${afterBeginFormInlineMarkup}<div id="vueture-backend-module-docheader-menu">${routerViewMarkup}</div>${beforeEndFormInlineMarkup}`;
    if (menuExists) {
      leftColumnElement.insertAdjacentHTML('beforeend', markup);
    } else {
      convertToRightElement.insertAdjacentHTML('beforebegin', markup);
    }
  }

  /**
   * Add dom elements for vue mounting
   * @param {boolean} withRouterView
   * @param {number} numberOfButtonGroupsLeft
   * @param {number} numberOfButtonGroupsRight
   */
  static generateVueDocHeaderButtonsMarkup(withRouterView = false, numberOfButtonGroupsLeft = 1, numberOfButtonGroupsRight = 1) {
    const leftButtonToolBarElement = document.querySelector('.module-docheader-bar-column-left > .btn-toolbar');
    const rightButtonToolBarElement = document.querySelector('.module-docheader-bar-column-right > .btn-toolbar');
    const beforeIdPrefix = 'vueture-backend-module-docheader-buttons-right-';
    const beforeRouteViewPrefix = 'docheaderButtonsRight';
    const afterIdPrefix = 'vueture-backend-module-docheader-buttons-left-';
    const afterRouteViewPrefix = 'docheaderButtonsLeft';
    let afterbeginMarkup = '';
    let beforeendMarkup = '';
    for (let i = 1; i <= numberOfButtonGroupsLeft; i++) {
      beforeendMarkup += `<div id="${afterIdPrefix + i}" class="btn-group vueture-btn-group" role="group" aria-label="">${withRouterView ? `<router-view name="${afterRouteViewPrefix + i}"></router-view>` : ''}</div>`;
    }
    for (let j = numberOfButtonGroupsRight; j > 0; j--) {
      afterbeginMarkup += `<div id="${beforeIdPrefix + j}" class="btn-group vueture-btn-group" role="group" aria-label="">${withRouterView ? `<router-view name="${beforeRouteViewPrefix + j}"></router-view>` : ''}</div>`;
    }
    rightButtonToolBarElement.insertAdjacentHTML('afterbegin', afterbeginMarkup);
    leftButtonToolBarElement.insertAdjacentHTML('beforeend', beforeendMarkup);
  }
}
