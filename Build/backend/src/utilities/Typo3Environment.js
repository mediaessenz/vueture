import Typo3Utilities from './Typo3Utilities';

export const pid = Typo3Utilities.determineSelectedPageId();
export const lang = Typo3Utilities.getTypo3LanguageLabels(TYPO3.lang);
// eslint-disable-next-line no-restricted-globals
export const notification = top.TYPO3.Notification;
// eslint-disable-next-line no-restricted-globals, prefer-destructuring
export const ajaxUrls = top.TYPO3.settings.ajaxUrls;
export const verbose = TYPO3.settings.vueture.verbose === '1';
export const debug = TYPO3.settings.vueture.debug === '1';
export const vueDevTools = TYPO3.settings.vueture.vueDevTools === '1';
export const consolePrefix = '[vueture]';
