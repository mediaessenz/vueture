import Vue from 'vue';
import VueRouter from 'vue-router';
import Vuex from 'vuex';
import { ServerTable } from 'vue-tables-2';

import Typo3Utilities from './utilities/Typo3Utilities';
import {
  notification, ajaxUrls, verbose, consolePrefix,
} from './utilities/Typo3Environment';

import router from './router';
import store from './store';

// devtools import is only needed for debugging
// import devtools from '@vue/devtools';
// devtools.connect();

import './styles/backend-module.scss';

Vue.use(VueRouter);
Vue.use(Vuex);
Vue.use(ServerTable, {}, true, 'bootstrap3', 'default');

document.addEventListener('DOMContentLoaded', () => {
  if (verbose) {
    // eslint-disable-next-line no-console
    console.log(`${consolePrefix} [loader] dom ready`);
  }

  fetch(ajaxUrls['vueture/route']).then(response => response.json()).then((responseJson) => {
    store.dispatch('routesMenuModule/CHANGE_ROUTE', responseJson.route);
  }).then(
    () => {
      Typo3Utilities.generateVueDocHeaderMenuMarkup(true);

      new Vue({
        router,
        store,
      }).$mount('#vueture-backend-module-docheader-menu');

      const numberOfButtonGroupsLeft = 1;
      const numberOfButtonGroupsRight = 2;

      Typo3Utilities.generateVueDocHeaderButtonsMarkup(true, numberOfButtonGroupsLeft, numberOfButtonGroupsRight);

      for (let n = 1; n <= numberOfButtonGroupsLeft; n++) {
        new Vue({
          router,
          store,
        }).$mount(`#vueture-backend-module-docheader-buttons-left-${n}`);
      }

      for (let n = 1; n <= numberOfButtonGroupsRight; n++) {
        new Vue({
          router,
          store,
        }).$mount(`#vueture-backend-module-docheader-buttons-right-${n}`);
      }

      // See Classes/Controller/BackendModuleController.php::addDocheaderButtons for more info
      // new Vue({
      //     methods: {
      //         onClick() {top.TYPO3.Modal.confirm('Info', 'this info comes from ...')}
      //     }
      // }).$mount('#vueture-backend-module-docheader-button-info');

      new Vue({
        router,
        store,
      }).$mount('#vueture-backend-module-content');

      // eslint-disable-next-line no-restricted-globals
      top.TYPO3.Backend.Loader.finish();
    }
  )
  .catch((error) => {
    notification.error('Backend module controller feedback', error);
  });

});
