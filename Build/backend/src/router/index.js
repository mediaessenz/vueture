import Vue from 'vue';
import Router from 'vue-router';

Vue.use(Router);
import RoutesMenu from '../components/RoutesMenu.vue';
import ButtonGroup from '../components/ButtonGroup.vue';
import HelloWorld from '../components/HelloWorld.vue';
import FilterExample from '../components/FilterExample.vue';
import FilterExample2 from '../components/FilterExample2.vue';
import store from '../store';

const buttonGroupInfoProps = {
  buttons: [
    {
      iconIdentifier: 'actions-document-info',
      emission: function () {
        let title = 'Backend Module Info';
        let message = 'This demo backend module was brought to you by MEDIA::ESSENZ';
        top.TYPO3.Modal.confirm(title, message, top.TYPO3.Severity.info, [
          {
            text: 'OK',
            btnClass: 'btn-info',
            name: 'ok'
          }]).on('button.clicked', function (e) {
          if (e.target.name === 'ok') {
            // eslint-disable-next-line no-console
            console.log('OK');
          }
          top.TYPO3.Modal.dismiss();
        });
      }
    }
  ]
};

const buttonGroupRefreshProps = {
  buttons: [
    {
      iconIdentifier: 'actions-refresh', emission: () => {
        store.dispatch('REFRESH');
      }
    }
  ]
};

const buttonGroupLogoSizeProps = {
  title: 'Change logo size +-100:',
  buttons: [
    {
      iconIdentifier: 'actions-add', emission: () => {
        store.commit('helloWorldModule/incrementLogoSize', 100);
      }
    },
    {
      iconIdentifier: 'actions-remove', emission: () => {
        store.commit('helloWorldModule/decrementLogoSize', 100);
      }
    }
  ]
};

const buttonGroupOneProps = {
  buttons: [
    {
      iconIdentifier: 'actions-document-save', emission: () => {
        store.dispatch('typo3HerosOneModule/SAVE_HEROS');
      }
    },
    {
      iconIdentifier: 'actions-document-new', emission: () => {
        store.dispatch('typo3HerosOneModule/ADD_NEW_HERO');
      }
    },
    {
      iconIdentifier: 'actions-filter', emission: () => {
        store.dispatch('typo3HerosOneModule/TOGGLE_HEROS_FILTER');
      }
    }
  ]
};

export default new Router({
  routes: [
    {
      path: '/',
      name: 'home',
      components: {
        routesMenu: RoutesMenu,
        docheaderButtonsRight1: ButtonGroup,
        docheaderButtonsRight2: ButtonGroup,
        content: HelloWorld
      },
      props: {
        docheaderButtonsRight1: buttonGroupInfoProps,
        docheaderButtonsRight2: buttonGroupRefreshProps,
        content: {
          title: 'Hello Vueture!',
          message: 'This is the default view of the vuejs demo.',
          logo: false
        }
      }
    },
    {
      path: '/example-1',
      name: 'example-1',
      components: {
        routesMenu: RoutesMenu,
        docheaderButtonsLeft1: ButtonGroup,
        docheaderButtonsRight1: ButtonGroup,
        docheaderButtonsRight2: ButtonGroup,
        content: HelloWorld
      },
      props: {
        docheaderButtonsLeft1: buttonGroupLogoSizeProps,
        docheaderButtonsRight1: buttonGroupInfoProps,
        docheaderButtonsRight2: buttonGroupRefreshProps,
        content: {
          title: 'Example 1',
          message: 'In this example you can change the logo size in different steps. The size will be stored in a vuex store.'
        }
      }
    },
    {
      path: '/example-2',
      name: 'example-2',
      components: {
        routesMenu: RoutesMenu,
        docheaderButtonsLeft1: ButtonGroup,
        docheaderButtonsRight1: ButtonGroup,
        docheaderButtonsRight2: ButtonGroup,
        content: FilterExample
      },
      props: {
        docheaderButtonsLeft1: buttonGroupOneProps,
        docheaderButtonsRight1: buttonGroupInfoProps,
        docheaderButtonsRight2: buttonGroupRefreshProps,
        content: {
          title: 'TYPO3 Heros V1',
          message: 'Please click the filter icon above to show list filter.',
          filter: store.state.showFilter
        }
      }
    },
    {
      path: '/example-3',
      name: 'example-3',
      components: {
        routesMenu: RoutesMenu,
        docheaderButtonsRight1: ButtonGroup,
        docheaderButtonsRight2: ButtonGroup,
        content: FilterExample2
      },
      props: {
        docheaderButtonsRight1: buttonGroupInfoProps,
        docheaderButtonsRight2: buttonGroupRefreshProps,
        content: {
          title: 'TYPO3 Heros V2',
          message: 'This example uses the server-table component of <a href="https://github.com/matfish2/vue-tables-2" target="_blank">https://github.com/matfish2/vue-tables-2</a>'
        }
      }
    }
  ]
});
