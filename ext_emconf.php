<?php

/***************************************************************
 * Extension Manager/Repository config file for ext "vueture".
 *
 * Auto generated 26-03-2018 12:26
 *
 * Manual updates:
 * Only the data in the array - everything else is removed by next
 * writing. "version" and "dependencies" must not be touched!
 ***************************************************************/

$EM_CONF['vueture'] = [
    'title' => 'Vueture',
    'description' => 'Template for using VueJS in TYPO3 Frontend/Backend',
    'version' => '2.3.1',
    'state' => 'beta',
    'author' => 'Alexander Grein',
    'author_email' => 'alexander.grein@gmail.com',
    'author_company' => 'MEDIA::ESSENZ',
    'constraints' =>
        [
            'depends' =>
                [
                    'typo3' => '10.4.0-10.4.99',
                    'typoscript_rendering' => '2.0.2-'
                ],
        ],
    'autoload' =>
        [
            'psr-4' =>
                [
                    'MEDIAESSENZ\\Vueture\\' => 'Classes',
                ],
        ]
];
