<?php

use MEDIAESSENZ\Vueture\Controller\BackendModuleAjaxController;

return [

    // simple
    'vueture/simple' => [
        'path' => '/vueture/simple',
        'target' => BackendModuleAjaxController::class . '::simpleAction'
    ],

    // Store (and restore if no route parameter is set) route for vue-router
    'vueture/route' => [
        'path' => '/vueture/route',
        'target' => BackendModuleAjaxController::class . '::routeAction'
    ],

    // Heros
    'vueture/heros' => [
        'path' => '/vueture/heros',
        'target' => BackendModuleAjaxController::class . '::herosAction'
    ],

    // Add Hero
    'vueture/hero/add' => [
        'path' => '/vueture/hero/add',
        'target' => BackendModuleAjaxController::class . '::AddHeroAction'
    ],

];
